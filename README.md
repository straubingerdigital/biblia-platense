[![wercker status](https://app.wercker.com/status/7a12dca501090c66e3afcb41768ae658/m/master "wercker status")](https://app.wercker.com/project/bykey/7a12dca501090c66e3afcb41768ae658)

# Misión #

El esfuerzo de este proyecto es el de brindar al publico una versión en digital de la famosa **Biblia Platense** o **Biblia Comentada**, traducción realizada a partir de los textos originales por Monseñor Dr. Juan Straubinger. Esta traducción se caracteriza por sus ricos y explicativos comentarios al pie de pagina que el lector podrá encontrar al pie de la mayoría de las paginas.

El equipo de Straubinger Digital considera que el acceso a tan preciado material debe estar al alcance de todos. Así como también evitar usar /formatos cerrados/ realizados con [software privativo](https://www.gnu.org/philosophy/proprietary/proprietary.html) como fuente para las versiones digitales de susodicho material. Es por eso que el trabajo de edición es realizado con [software libre](https://www.gnu.org/philosophy/free-sw.html), lo que garantiza que futuras generaciones de siglos por venir podrán trabajar con el material fuente y crear copias de esta Biblia para los formatos que existan en el futuro (algo que **no se puede garantizar** si el documento fuente esta en formato Microsoft Word, ya que Microsoft podría no existir más en 100 años haciendo sus productos inaccesibles y los archivos creados con ellos ilegibles).

# Descargar una copia #

Este proyecto esta apenas en su infancia, pero una copia en formato PDF del progreso actual es ofrecido para descargar. Visite la sección [Descargas](https://bitbucket.org/straubingerdigital/biblia-platense/downloads) para más información.
